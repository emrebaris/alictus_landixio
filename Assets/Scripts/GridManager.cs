﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour {

    public void FillGrid(List<Vector3> positions)
    {
        foreach(Vector3 position in positions)
        {
            Instantiate(filledGrid, position, Quaternion.identity);
        }
    }

    public GameObject filledGrid;
}
