﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum Direction
{
    Up,
    Down,
    Left,
    Right
}

public class PlayerController : MonoBehaviour
{
    void Awake()
    {
        gridManager = GameObject.FindGameObjectWithTag("Grid").GetComponent<GridManager>();

        GoRight();
        StartCoroutine("Move");
    }

    IEnumerator Move()
    {
        while (true)
        {
            Vector3 nextPosition = transform.position + movementDirection;

            GameObject circleGameObject = new GameObject();
            SpriteRenderer spriteRenderer = circleGameObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = circle;
            circleGameObject.transform.position = nextPosition;
            spawnedCircles.Add(circleGameObject);
            travelPositions.Add(nextPosition);

            transform.position = nextPosition;

            yield return new WaitForSeconds(0.25f + (100.0f - movementSpeed) / 100.0f);
        }
    }

    public void GoUp()
    {
        if (direction != Direction.Down)
        {
            direction = Direction.Up;
            movementDirection = new Vector3(0, 0.24f, 0);
        }
    }

    public void GoDown()
    {
        if (direction != Direction.Up)
        {
            direction = Direction.Down;
            movementDirection = new Vector3(0, -0.24f, 0);
        }
    }

    public void GoLeft()
    {
        if (direction != Direction.Right)
        {
            direction = Direction.Left;
            movementDirection = new Vector3(-0.24f, 0, 0);
        }
    }

    public void GoRight()
    {
        if (direction != Direction.Left)
        {
            direction = Direction.Right;
            movementDirection = new Vector3(0.24f, 0, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "FilledGrid")
        {
            isTravelling = false;

            foreach (GameObject circle in spawnedCircles)
            {
                Destroy(circle);
            }

            spawnedCircles.Clear();

            gridManager.FillGrid(travelPositions);
            travelPositions.Clear();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Grid")
        {
            SceneManager.LoadScene(0);
        }
        else if (other.tag == "FilledGrid")
        {
            isTravelling = true;
            //isExitingAFilledGrid = true;
        }
    }

    public Sprite circle;

    [Range(1.0f, 100.0f)]
    public float movementSpeed = 100.0f;
    private Direction direction = Direction.Right;

    private Vector3 movementDirection;

    private List<GameObject> spawnedCircles = new List<GameObject>();
    private List<Vector3> travelPositions = new List<Vector3>();

    private GridManager gridManager;

    bool isExitingAFilledGrid = false;
    bool isEnteringAFilledGrid = false;

    bool isTravelling = false;
}
