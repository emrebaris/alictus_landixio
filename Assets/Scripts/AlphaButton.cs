﻿using UnityEngine;
 using System.Collections;
 using UnityEngine.UI; // Required when Using UI elements.
 
 public class AlphaButton : MonoBehaviour
 {
     public float alphaThreshold = 0.1f;
 
     void Start()
     {
         gameObject.GetComponent<Image>().alphaHitTestMinimumThreshold = alphaThreshold;
     }
 }